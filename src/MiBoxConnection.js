const dgram = require('dgram');

class MiBoxConnection{
	constructor(ip, port=5987){
		this.ip = ip;
		this.port = port;

		this.lightType = "rgbww";
		this.group = 1;

		//turn off all the lights when the lightness value of the setRgb method reaches below this value, use a negative value to disable this feature
		this.rgbOffStateThreshold = 0.1;

		//use kelvin mode when saturation reaches below this value, use a negative value to disable this feature
		//milights will always show the rgb leds, even when setting the rgb to white. This feature puts the milights
		//in kelvin mode to disable the faint red tint when your pass a white color in the setRgb method
		this.rgbKelvinSaturationThreshold = 0.1;

		this.commandCounter = 0;
		this.sessionId1 = 0;
		this.sessionId2 = 0;
		this.client = dgram.createSocket("udp4");

		this.onMessageCbs = [];
		this.onMessageDisposableCbs = [];
		this.client.on("message", message => {
			for(const cb of this.onMessageCbs){
				cb(message);
			}
			for(const cb of this.onMessageDisposableCbs){
				cb(message);
			}
			this.onMessageDisposableCbs = [];
		});
	}

	sendMsg(msg){
		this.client.send(Buffer.from(msg), 0, msg.length, 5987, "192.168.1.5");
	}

	async sendCmd(cmd){
		this.commandCounter++;
		if(this.commandCounter > 255){
			this.commandCounter = 0;
		}

		if(this.sessionId1 == 0 && this.sessionId2 == 0){
			let gotSessionId = await this.getSessionId();
			if(!gotSessionId) return false;
		}

		let msg = [0x80,0x00,0x00,0x00,0x11, this.sessionId1, this.sessionId2, 0x00, this.commandCounter, 0x00];
		let headersLength = msg.length;
		msg.push(0x31,0x00,0x00)
		if(this.lightType == "rgbww"){
			msg.push(0x08);
		}else if(this.lightType == "rgbw"){
			msg.push(0x07);
		}else{
			msg.push(0x00);
		}
		msg.push(...cmd);
		msg.push(this.group);
		msg.push(0x00);

		//calculate checksum
		let crc = 0;
		for(let i=0; i<msg.length - headersLength; i++){
			crc = (crc + msg[headersLength+i]) & 255;
		}
		msg.push(crc);
		this.sendMsg(msg);
		return true;
	}

	async getSessionId(){
		this.sendMsg([0x20, 0x00, 0x00, 0x00, 0x16, 0x02, 0x62, 0x3A, 0xD5, 0xED, 0xA3, 0x01, 0xAE, 0x08, 0x2D, 0x46, 0x61, 0x41, 0xA7, 0xF6, 0xDC, 0xAF, 0xD3, 0xE6, 0x00, 0x00, 0x1E]);
		let totalTries = 0;
		while(totalTries < 3){
			totalTries++;
			let data = await this.waitForMessage();
			this.sessionId1	= data[19];
			this.sessionId2	= data[20];
			return true;
		}
		return false;
	}

	async waitForMessage(){
		return await new Promise(r => this.onMessageDisposableCbs.push(r));
	}

	async sendOnCmd(){
		let cmd = [];
		if(this.lightType == "rgbww"){
			cmd = [0x04,0x01];
		}else if(this.lightType == "rgbw"){
			cmd = [0x03,0x01];
		}else{
			cmd = [0x03,0x03];
		}
		cmd.push(0x00,0x00,0x00);
		return await this.sendCmd(cmd)
	}

	async sendOffCmd(){
		let cmd = [];
		if(this.lightType == "rgbww"){
			cmd = [0x04,0x02];
		}else if(this.lightType == "rgbw"){
			cmd = [0x03,0x02];
		}else{
			cmd = [0x03,0x04];
		}
		cmd.push(0x00,0x00,0x00);
		return await this.sendCmd(cmd)
	}

	//brightness is between 0-100
	async sendBrightnessCmd(brightness){
		brightness = Math.round(brightness);
		let cmd = [];
		if(this.lightType == "rgbww"){
			cmd = [0x03];
		}else if(this.lightType == "rgbw"){
			cmd = [0x02];
		}else{
			cmd = [0x02];
		}
		cmd.push(brightness);
		cmd.push(0x00,0x00,0x00);
		return await this.sendCmd(cmd);
	}

	//hue is between 0-255
	async sendHueCmd(hue){
		hue = Math.round(hue);
		return await this.sendCmd([0x01,hue,hue,hue,hue]);
	}

	//saturation is between 0-100
	async sendSaturationCmd(saturation){
		saturation = Math.round(saturation);
		let cmd = [0x02];
		//todo: not sure if \x02 works with rgbw and hub lights,
		//I don't have the hardware so I can't test which bytes are needed here
		cmd.push(saturation);
		cmd.push(0x00,0x00,0x00);
		return await this.sendCmd(cmd);
	}

	//kelvin is between 0-100
	async sendKelvinCmd(kelvin){
		kelvin = Math.round(kelvin);
		return await this.sendCmd([0x05, kelvin, 0x00, 0x00, 0x00]);
	}

	//https://gist.github.com/mjackson/5311256
	rgbToHsv(r, g, b) {
		r /= 255, g /= 255, b /= 255;

		var max = Math.max(r, g, b), min = Math.min(r, g, b);
		var h, s, v = max;

		var d = max - min;
		s = max == 0 ? 0 : d / max;

		if (max == min) {
			h = 0; // achromatic
		} else {
			switch (max) {
				case r: h = (g - b) / d + (g < b ? 6 : 0); break;
				case g: h = (b - r) / d + 2; break;
				case b: h = (r - g) / d + 4; break;
			}

			h /= 6;
		}

		return [ h, s, v ];
	}

	async setRgb(r,g,b){
		let [h,s,v] = this.rgbToHsv(r,g,b);
		if(v <= this.rgbOffStateThreshold){
			await this.sendHueCmd(10);
			await this.sendSaturationCmd(0);
			await this.sendBrightnessCmd(9);
			await this.sendOffCmd();
		}else{
			await this.sendOnCmd();
			if(s <= this.rgbKelvinSaturationThreshold){
				await this.sendBrightnessCmd(v*100);
				await this.sendKelvinCmd(100);
			}else{
				await this.sendHueCmd(h*255);
				await this.sendSaturationCmd((1-s)*100);
				await this.sendBrightnessCmd(v*100);
			}
		}
	}
}

module.exports.MiBoxConnection = MiBoxConnection;
