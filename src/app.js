const { spawn } = require('child_process');
const path = require('path');
const dgram = require('dgram');
const { MiBoxConnection } = require("./MiBoxConnection.js");

function hexToRgb(hex) {
	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	} : null;
}

(async _ => {
	let connection = new MiBoxConnection("192.168.1.5");
	await connection.sendOnCmd();

	const batPath = path.join(__dirname, 'captureScreen.bat')
	const captureScreen = spawn(batPath, [], {
		cwd: __dirname,
	});

	captureScreen.stdout.on('data', (data) => {
		let str = data.toString();
		str = str.trim();
		let rgb = hexToRgb(str);
		if(rgb){
			connection.setRgb(rgb.r, rgb.g, rgb.b);
		}
	});

	captureScreen.stderr.on('data', (data) => {
		console.error(`stderr: ${data}`);
	});

	captureScreen.on('close', (code) => {
		console.log(`child process exited with code ${code}`);
	});
})();

process.stdin.resume();
