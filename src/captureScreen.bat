// 2>nul||@goto :batch
/*
:batch
@echo off
setlocal

:: find csc.exe
set "csc="
for /r "%SystemRoot%\Microsoft.NET\Framework\" %%# in ("*csc.exe") do  set "csc=%%#"

if not exist "%csc%" (
   echo no .net framework installed
   exit /b 10
)

if exist "%~n0.exe" (
   call %csc% /nologo /r:"Microsoft.VisualBasic.dll" /win32manifest:"app.manifest" /out:"%~n0.exe" "%~dpsfnx0" || (
      exit /b %errorlevel%
   )
)
%~n0.exe %*
endlocal & exit /b %errorlevel%

*/

// reference
// https://gallery.technet.microsoft.com/scriptcenter/eeff544a-f690-4f6b-a586-11eea6fc5eb8

using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.VisualBasic;

public class CaptureScreen{


    public static void Main(){
        RECT windowRect = new RECT();
        GetWindowRect(GetDesktopWindow(), ref windowRect);
        Bitmap screenPixel = new Bitmap(windowRect.right, windowRect.bottom, PixelFormat.Format32bppArgb);
        Graphics gdest = Graphics.FromImage(screenPixel);
        Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero);
        while(true){
            Stopwatch sw = new Stopwatch();
            sw.Start();
            IntPtr hSrcDC = gsrc.GetHdc();
            IntPtr hDC = gdest.GetHdc();
            BitBlt(hDC, 0, 0, windowRect.right, windowRect.bottom, hSrcDC, 0, 0, (int)CopyPixelOperation.SourceCopy);
            gdest.ReleaseHdc();
            gsrc.ReleaseHdc();
            float resolutionMultiplier = 0.01f;
            long avgR = 0;
            long avgG = 0;
            long avgB = 0;
            int pixelCount = 0;
            for(float x=0; x<windowRect.right; x+=1/resolutionMultiplier){
                for(float y=0; y<windowRect.bottom; y+=1/resolutionMultiplier){
                    Color c = screenPixel.GetPixel(Convert.ToInt32(x), Convert.ToInt32(y));
                    avgR += c.R;
                    avgG += c.G;
                    avgB += c.B;
                    pixelCount++;
                }
            }
            Color avgColor = Color.FromArgb((int)(avgR/pixelCount), (int)(avgG/pixelCount), (int)(avgB/pixelCount));
            Thread.Sleep(100);
            sw.Stop();
            Console.WriteLine(ColorTranslator.ToHtml(Color.FromArgb(avgColor.ToArgb())));
            // Console.WriteLine("Elapsed={0}",sw.Elapsed);
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT{
        public int left;
        public int top;
        public int right;
        public int bottom;
    }

    [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
    public static extern int BitBlt(IntPtr hDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);
    [DllImport("user32.dll")]
    public static extern IntPtr GetDesktopWindow();
    [DllImport("user32.dll")]
    public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);
}
